Computational Physics I Take-home Final Exam

Question 1: Disease Modeling.
Solve two coupled ordinary diffential equations; for I, number of infectious members of the population;  S, susceptible members of the population. For this question I implimented from scratch, without textbook or internet, a sparse RK45 solver in python. For this, I only imported as single math function: math.log10. All other aspects, other than plotting were implimented from scratch.

Question 2:
Establish the numeric stability criterion for a modified Crank-Nicolson method.

Question 3: Diffusion of a radiogenic isotope with cooling.
Solve the time-independent solution of a differential equation describing the concentration of an isotope of Potassium, in a grain of the mineral hornblende. Concentration was a function of time, as the isotope was produced and decayed away; and of temperature, which increased its diffusion out of the grain. Interpret the results of your simulation, given different conditions. For this question, I modified and combined several Octave (Matlab) codes which had been given to the class, in order to solve the partial differential equation.
