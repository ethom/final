%% PHY 5340 Final Exam
%  Question 3
N = 100;
TD = 3*10^4;
A0 = 33.76;
T0 = 1000;
tau = 30*1e6;
a = 0.5*10^(-3);
x0=0; x1=a;
le = 5.81*10^-11;
ltot = 5.543*(10^(-10));
C_k = 1;
T = @(t) T0./(1+t./tau);
D = @(t) A0.*exp(-TD./T(t));
p = @(t) le.*C_k.*exp(-ltot.*t);
Css = p(0)*a^2/D(0)/6;
Wss50 = p(0)*a^3*(3/(6*8))/D(0)

h=(x1-x0)/N; %spacing of intervals in x direction. 
dt=10;
lambda=@(t,dt) D(t).*dt./h^2;
ltot = 5.543*10^-10;


x=x0:h:x1; %array of x points (size (1,N))
x=x';
%remove boundary points, where we already know the solution
x(1)=[]; x(end)=[];
u=zeros(size(p(t).*x));
u(end)=0;
u0 = u;

%% Crank-Nicolson Matrices
I=eye(N-1);
L=toeplitz([-2 1 zeros(1,N-3)]);
A_CN=@(t,dt) I + lambda(t,dt)./2.*(L); %Crank-Nicolson
B_CN=@(t,dt) I - lambda(t,dt)./2.*(L); %Crank-Nicolson

%% While Loop Init
t = 0;
tmax=100000000;
%dif = abs(Wss50-u(50))
tl = t;
C = u;
Cl = C;
while t < tmax
	t
	dt*=1.1;
	for tims=0:100
		t+=dt;
		B = B_CN(t,dt);
		A = A_CN(t,dt);
		pat = p(t);
		u=(B\((A*u) + dt.*x.*pat)); %Crank-Nicolson propagator
	end
	%dif = abs(Wss50-u(50));
	C = u(1:end)./x(1:end);
	Cl=[Cl,C];
	tl=[tl,t];
end

%% Plotting for Steady State
%fig = figure;
%hold on;
%plot(x,u);
%title("W in Steady State");
%xlabel("radius");
%ylabel("W");
%saveas(fig,"Wss.png");
%hold off;
%close all;

%fig = figure;
%hold on;
%plot(x,C);
%title("C in Steady State");
%xlabel("radius");
%ylabel("C");
%saveas(fig,"Css.png");
%hold off;
%close all;

%%Plotting for Time Evolution.
fig2 = figure;
hold on;
semilogy(x,Cl(:,1))
leg=[num2str(tl(1))];
for i = 2:25:min(size(Cl))
	i
	semilogy(x,Cl(:,i))
	leg=[leg;num2str(tl(i))]
	pause(0.5)
end
title(["C at Various Times: Tau Factor 2"]);
legend(leg)
xlabel("radius");
ylabel("Concentration");
saveas(fig2,"Covertime2.png");
hold off;
