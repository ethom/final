from math import log10 
import matplotlib as mt
import matplotlib.pyplot as plt
import time


## Sparse RK45
#
# RUNGEKUTTA-FELBERGS BUTCHER TABLEAU
A_num = [[1],[3,9],[1932,-7200,7296],[429,-8,3680,-845],[-8,2,-3544,1859,-11]]
A_deno = [[4],[32,32],[2197,2197,2197],[216,1,513,4104],[27,2,2565,4104,40]]
A_divi = [4,32,2197,4104,20520]
A_denofac = [[A_divi[i]//A_deno[i][j] for j in range(len(A_deno[i]))] for i in range(5)] #// int division 
A_multi = [[A_num[i][j]*A_denofac[i][j] for j in range(i+1)] for i in range(5)]
#
# COLUMN OF RK-FELBERG BUTCHER TABLE
c = [0,1/4,3/8,12/13,1,1/2]
#
# BOTTOM PART OF RK-Felberg method
b5 = [16/135,0,6656/12825,28561/56430,-9/50,2/55]
b4 = [25/216,0,1408/2565 ,2197/4104  ,-1/5 ,0]
#
##

## Derivative Function
#
def f_func(u,params):
    #
    # Assign parameters
    I = u[0]
    S = u[1]
    beta = params[0]
    N = params[1]
    gamma = params[2]
    #
    # Calculate derivatives
    It = (beta*(S/N) - gamma)*I
    St = -beta*((S*I)/N)
    return [It,St]
#
##


def rk45(u,params,dt,t):
    fn = [f_func(u,params)] 
    ## fn 
    #   > holds outputs from functions (f_func(u,params) = du)
    #   > expands for each calculation of K_i required for RK45

    #
    # K_i calculation loop 
    for i in range(5):
        u_prime = [u[k]+dt*sum([fn[j][k]*A_multi[i][j] for j in range(i+1)])/A_divi[i] for k in range(len(u))]
        #
        # k - u index, range of 2
        # j - A matrix index, range of i+1 >> range expands each calculation of k_i
        # i - rk45, kappa index.
        el = []
        fn.append(f_func(u_prime,params))
    # 
    # Calculated u4, u5
    u4 = [u[j] + dt*(sum([b4[i]*fn[i][j] for i in range(len(b4))])) for j in range(2)]
    u5 = [u[j] + dt*(sum([b5[i]*fn[i][j] for i in range(len(b5))])) for j in range(2)]
    err = sum([(u4[i]/u5[i] - 1)**2 for i in range(2)])
    #print("t :",t)
    #print("dt: ",dt)
    #print("b4: :",b4)
    #print("b5: :",b5)
    #print("err: ",err)
    #print("~~~")
    #time.sleep(0.05)
    return u5,err

    
def update(u,params,dt,t,learning_rate,error_tolerance):
    #
    # Receive update with error, given dt.
    u5,err = rk45(u,params,dt,t)
    #
    # Error error_tolerance test
    if int(err < error_tolerance):
        # Then update
        u = u5
        t+=dt
        dt *= learning_rate*(error_tolerance/err)**(1/5)
    else:
        # Then half the time-step and return t,u unchanged.
        dt /= 2
    return u,t,dt,err


def main(b,N,y,I0,S0,t0,dumi_tlk,tmax,dt,learning_rate,error_tolerance):
    #
    # Initialize
    u = [I0,S0]
    params = [b,N,y]
    t = t0 
    r = 0
    #dt = 0.00001
    #
    # Parameters
    #
    # Output Lists
    ul = [u,u]
    tl = [t,t]
    el = []
    #
    # Update Loop
    while ( (tmax == 200) and abs(((-(ul[-1][0]-ul[-2][0])-(ul[-1][1]-ul[-2][1]))) > 1*10**(-(5-(int(log10(r+0.0001)) + 1))))) or t < tmax - dt:
        #[[[1)            and                    2)]                               >                   3)]                        or [     4)    ]]
        #
        #1) If this is a finishing main call (uncontrolled and second half of controlled)
        #       > since we are only concerned about tf if it is on the tail of the pandemic
        #AND
        #2) the difference of Rl between subsequent steps is greater than 3)
        #       > the defining characteristic of tf
        #3) The moving target of 5 significant digits 
        #       >since the number of digits in t increases
        #
        #OR
        #
        # 4) t_i belongs to the front of the pandemic
        #
        u,t,dt,err = update(u,params,dt,t,learning_rate,error_tolerance)
        tl.append(t)
        ul.append(u)
        el.append(err)
    #
    # Final step
    if t < tmax -dt:
        dt = tmax-t
        u,t,dt,err = update(u,params,dt,t,learning_rate,error_tolerance)
        r = N - u[-1][0] - u[-1][1]
        #dr = - (u[-1][0] - u[-1][0]) - (u[-1][1] - u[-1][1])
        tl.append(t)
        ul.append(u)
        el.append(err)
    #
    # Split of Infected/Susceptible records.
    Il = [ul[i][0] for i in range(1,len(ul))]
    Sl = [ul[i][1] for i in range(1,len(ul))]
    tl = tl[1:]
    return [Il,Sl,tl,dt,el,tl[-1]]
#
## Additional utilities
def table_report(bf,tmaxf,Nf,Ilf,Slf,tlf,dtf):
    tf = tlf[-1]
    if bf == 0.25:
        print("                                                   ")
        print("                  Table Reporting                  ")
        print(" _________________________________________________ ")
        print("|                                                 |")
        if tmaxf > 60:
            print("|~~~~~~ Uncontrolled Case: beta = 0.25 /day ~~~~~~|")
            #
            #
            print("|_________________________________________________|")
            #pdum="| t_final = "+str(t)
            pdum="| t_final = "+str(round(tf,5-(int(log10(tf)) + 1)))
            print(pdum+" "*(50-len(pdum))+"|")
            #
            ## Total number of people infected
            print("|_________________________________________________|")
            pnum = int(N-Slf[-1])
            pdum="| Total number of people infected    = "+str(pnum)
            print(pdum+" "*(50-len(pdum))+"|")
            #
            #
            print("|_________________________________________________|")
            pnum = round(tlf[Ilf.index(max(Ilf))],3)
            pdum="| Day of largest infected population = "+str(pnum)
            print(pdum+" "*(50-len(pdum))+"|")
            #
            #
            print("|_________________________________________________|")                  
        else:
            print("|~~~~~~~ Controlled Case: beta = 0.05 /day ~~~~~~~|")
            print("|_________________________________________________|")                  
    if bf == 0.05:
        lockdown_ind = Ilf.index(max(Ilf))
        N_infected_beginning = Nf - Slf[0] #R + I = number of people infected at t
        N_infected_end = Nf - Slf[-1]
        N_infected_after_t = N_infected_end-N_infected_beginning 
        #
        #
        pdum="| t_final = "+str(round(tf,5-(int(log10(tf)) + 1)))
        print(pdum+" "*(50-len(pdum))+"|")
        #
        #
        print("|_________________________________________________|")
        pnum = int(N-Slf[-1])
        pdum="| Total number of people infected    = "+str(pnum)
        print(pdum+" "*(50-len(pdum))+"|")
        #
        #
        print("|_________________________________________________|")
        pnum = round(tlf[Ilf.index(max(Ilf))],3)
        pdum="| Day of largest infected population = "+str(pnum)
        print(pdum+" "*(50-len(pdum))+"|")
        #
        #
        print("|_________________________________________________|")                  
        pnum = int(N_infected_after_t)
        pdum="| Number of people infected post lockdown = "+str(pnum)
        print(pdum+" "*(50-len(pdum))+"|")
        print("|_________________________________________________|")
        #
        #
        print("                                                   ")

def missized_flatten(a,b):
    fl = []
    try:
        c = [a.error_toleranceist(),b.error_toleranceist()]
    except:
        c = [a,b]
    for i in range(len(c)):
        for j in range(len(c[i])):
            fl.append(c[i][j])
    return fl
#
def Quarantine(b,N,y,I0f,S0f,t0f,tlkf,tmaxf,dt0f,learning_ratef,error_tolerancef):
    [Il1f, Sl1f, tl1f, dtf, elf1, tf] = main(b,N,y,3,N-3,0,"dumi_tlk",tlkf,dt0f,learning_ratef,error_tolerancef)
    [Il2f, Sl2f, tl2f, dtf, elf2, tf] = main(0.05,N,y,Il1f[-1],Sl1f[-1],tl1f[-1],"dumi_tlk",tmaxf,dtf,learning_ratef,error_tolerancef)
    Ilf = missized_flatten(Il1f,Il2f[1:])
    Slf = missized_flatten(Sl1f,Sl2f[1:])
    tlf = missized_flatten(tl1f,tl2f[1:])
    elf = missized_flatten(tl1f,tl2f[1:])
    return [Ilf, Slf, tlf, dtf,elf, tlf[-1]]
#
##

tic = time.time()
error_tolerance = 0.0005
N = 37*10**6
dt0 = 0.005
b = 0.25
y = 0.1
learning_rate = 0.9
tlk = 60
### Uncontrolled

[Il, Sl, tl, dt, el, tf] = main(b,N,y,3,N-3,0,"dumi_tlk",200,dt0,learning_rate,error_tolerance)
table_report(b,200,N,Il,Sl,tl,dt)
Rl = [N - Il[i] - Sl[i] for i in range(len(Il))]
tfu = tf

# Semi-log plot
fig = plt.figure()
plt.semilogy(tl,Sl)
plt.semilogy(tl,Il)
plt.semilogy(tl,Rl)
plt.legend(["Susceptible","Infected","Recovered"])
plt.title("Uncontrolled Disease Spread")
plt.xlabel("Time")
plt.ylabel("Log(N)")
fig.savefig("DiseaseProgression_Uncontrolled.png")
plt.show()
plt.close()

fig = plt.figure()
plt.plot(tl[1:],[tl[i]-tl[i-1] for i in range(1,len(tl))])
plt.xlabel("Time")
plt.ylabel("Time Step")
plt.title("Time Step over Time: Uncontrolled")
plt.savefig("timestep_uncontrolled.png")
plt.show()
plt.close()

### Controlled 
[Il1, Sl1, tl1, dt, el1, tf] = main(b,N,y,3,N-3,0,"dumi_tlk",tlk,dt0,learning_rate,error_tolerance)
table_report(b,tlk,N,Il1,Sl1,tl1,dt)
[Il2, Sl2, tl2, dt, el2, tf] = main(0.05,N,y,Il1[-1],Sl1[-1],tl1[-1],"dumi_tlk",200,dt,learning_rate,error_tolerance)
table_report(0.05,200,N,Il2,Sl2,tl2,dt)
#table_report(b,200,N,Il,Sl,tl,dt)


Il = missized_flatten(Il1,Il2[1:])
Sl = missized_flatten(Sl1,Sl2[1:])
tl = missized_flatten(tl1,tl2[1:])
el = missized_flatten(el1,el2[1:])
Rl = [N-Il[i]-Sl[i] for i in range(len(Il))]
tfc = tf
#Il, Sl, tl, dt = main(0.25,N,0.1,3,N-3,0,200,dt0)

fig = plt.figure()
plt.semilogy(tl,Sl)
plt.semilogy(tl,Il)
plt.semilogy(tl,Rl)
plt.legend(["Susceptible","Infected","Recovered"])
plt.title("Controlled Disease Spread")
plt.xlabel("Time")
plt.ylabel("Log(N)")
fig.savefig("DiseaseProgression_Controlled.png")
plt.show()
plt.close()

fig = plt.figure()
plt.plot(tl[1:],[tl[i]-tl[i-1] for i in range(1,len(tl))])
plt.xlabel("Time")
plt.ylabel("Time Step")
plt.title("Time Step over Time: Controlled")
plt.savefig("timestep_controlled.png")
plt.show()
plt.close()

#
## Sensitivity analysis
for sensi in [[main,"Uncontrolled",tfu],[Quarantine,"Controlled",tfc]]:
    sfunc = sensi[0]
    slab = sensi[1]
    stf = sensi[2]
    #
    #
    varil = [0.75,0.775,0.8,0.825,0.85,0.875,0.9,0.925,0.95,0.975,0.99,0.999]
    varl = [sfunc(b,N,y,3,N-3,0,tlk,200,dt0,vari,error_tolerance)[-1] for vari in varil]
    plt.figure()
    plt.plot(varil,varl)
    plt.plot(varil,[stf]*len(varil))
    plt.legend(["Test Cases","Calculated Value"])
    plt.title("Variation wrt. Learning Rate of: "+slab)
    plt.xlabel("learning rate")
    plt.ylabel("Tf")
    plt.savefig("Var_wrt_lr"+slab+".png")
    plt.show()
    plt.close()
    
    ## Error Tolerance
    plt.figure()
    varil = [0.001,0.00075,0.0005,0.00025,0.0001,0.000075,0.00005,0.000025,0.00001]
    vartol= [sfunc(b,N,y,3,N-3,0,tlk,200,dt0,learning_rate,vari)[-1] for vari in varil]
    plt.plot(varil,vartol)
    plt.plot(varil,[stf]*len(varil))
    plt.legend(["Test Cases","Calculated Value"])
    plt.title("Variation wrt. Error Tolerance: "+slab)
    plt.xlabel("Error Tolerance")
    plt.ylabel("Tf")
    plt.savefig("Var_wrt_tol"+slab+".png")
    plt.show()
    plt.close()
    
    ## Initial time step
    plt.figure()
    varil = [0.01,0.0075,0.005,0.0025,0.001,0.0005,0.0001,0.00005,0.00001,0.0000075,0.000005,0.0000025,0.000001]
    vardt = [sfunc(0.25,N,0.1,3,N-3,0,tlk,200,vari,learning_rate,error_tolerance)[-1] for vari in varil]
    plt.plot(varil,vardt)
    plt.plot(varil,[stf]*len(varil))
    plt.legend(["Test cases","Calculated Value"])
    plt.title("Variation wrt. Initial Time Step: " + slab)
    plt.xlabel("Initial Time Step")
    plt.ylabel("Tf")
    plt.savefig("Var_wrt_dt"+slab+".png")
    plt.show()
    plt.close()
#
##

#
##
mxInf_l = [N-Quarantine(b,N,y,3,N-3,0,tlkv,200,dt0,learning_rate,error_tolerance)[1][-1] for tlkv in range(10,150)]
plt.figure()
plt.semilogy(range(10,150),mxInf_l)
plt.title("Time Prior to Quarantine wrt. Total People Infected")
plt.xlabel("Days Prior to Quarantine")
plt.ylabel("Number of People Infected")
plt.savefig("quarantine_variation.png")
plt.show()
plt.close()




